- if you call the function several times, augment the delay ?
- add metrics ?
- add redis features to the runtimes
- add redis discovery (to help function to call each other)
- right now, the system kill functions after a grace delay
  => augment the delay
  => kill the funktion if it's not use since a long time
  => the store do not call the funktions, it's the regulator
     => the regulator should tell the store to kill a funktion that is not used
  => if the execution time is to long, kill it