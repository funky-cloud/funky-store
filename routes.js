// TODO:
//  - add route to remove function and refresh the list

const rf = require('./read-functions')
const af = require('./add-function')
const sf = require('./start-function')
const constants = require('./constants')


async function routes (fastify, options) {
  
  fastify.log.info(`🤖 options routes:`)
  fastify.log.info(options)
  
  /*
  curl http://localhost:8090/funktions/list
   */
  fastify.get(`/${options.funktionsStorePrefix}/list`, async (request, reply) => {
    if(request.headers["funky-store-token"]===constants.funktionsStoreToken) {
      return JSON.stringify(await rf.getFunctionsJsonList(), null, 2)
    } else {
      return {error: "😡 bad credentials"}
    }
  })
  
  /*
  curl http://localhost:8090/funktions/text_list
   */
  // TODO: create a CORS version of this service
  fastify.get(`/${options.funktionsStorePrefix}/text_list`, async (request, reply) => {
    if(request.headers["funky-store-token"]===constants.funktionsStoreToken) {
      return rf.getFunctionsTable()
    } else {
      return {error: "😡 bad credentials"}
    }
  })
  
  /* ===================================
    Get information about the function
    curl http://localhost:8090/funktions/hello-kotlin
  =================================== */
  fastify.get(`/${options.funktionsStorePrefix}/:name`, async (request, reply) => {
    // TODO: check id return is null
    if(request.headers["funky-store-token"]===constants.funktionsStoreToken) {
  
      let result = rf.funktionsProjects[request.params.name]
      if(result===undefined) {
        return {error: "😡 unknown function" }
      } else {
        return {
          file: result.file,
          language: result.language,
          extension: result.extension,
          port: result.port,
          pid: result.pid,
          running: result.running,
          startedAt: result.startedAt,
          lastCall: result.lastCall,
          sinceLastCall: result.sinceLastCall
        }
      }
      
    } else {
      return {error: "😡 bad credentials"}
    }
    
  })
  
  /* ===================================
    kill a function
    TODO: how to get the CPU usage?
  =================================== */
  fastify.get(`/${options.funktionsStorePrefix}/kill/:name`, async (request, reply) => {
    // TODO: check id return is null
    if(request.headers["funky-store-token"]===constants.funktionsStoreToken) {
      let funktionName = request.params.name
      let result = rf.funktionsProjects[funktionName]
      if(result===undefined) {
        return {error: "😡 unknown function" }
      } else {
        result.child.kill()
        rf.funktionsProjects[funktionName].running = false
        rf.funktionsProjects[funktionName].pid = ""
        rf.funktionsProjects[funktionName].startedAt = ""
        rf.funktionsProjects[funktionName].sinceLastCall = ""
    
        return {message: `😢 ${request.params.name} function is killed` }
      }
    } else {
      return {error: "😡 bad credentials"}
    }
    
  })
  
  /* ===================================
    Add a function to the store
  =================================== */
  // TODO: validate the data
  //  - cf https://github.com/fastify/fastify/blob/master/docs/Getting-Started.md#validate-your-data
  fastify.post(`/${options.funktionsStorePrefix}`, async (request, reply) => {
  
    if(request.headers["funky-store-token"]===constants.funktionsStoreToken) {
      af.addFunction(request.body, fastify).then(projectInfo =>{
        reply.send(projectInfo)
      }).catch(error => {
        reply.send(error)
      })
    } else {
      return {error: "😡 bad credentials"}
    }

  })
  
  /* ===================================
    Start the runtime of the function
    curl http://localhost:8090/funktions/start/hello-kotlin
  =================================== */
  fastify.get(`/${options.funktionsStorePrefix}/:name/start`, async (request, reply) => {
  
    /*
    if(request.headers["funky-store-token"]===constants.funktionsStoreToken) {
    
    } else {
      return {error: "😡 bad credentials"}
    }
    
     */
    
    
    // started at
    sf.startFunction(request.params.name, fastify).then(projectInfo =>{
      reply.send(projectInfo)
    }).catch(error => {
      reply.send(error)
    })
  })
  
}

module.exports = routes
