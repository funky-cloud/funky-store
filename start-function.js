
/*
  TODO:
     - fork instead spawn?
     - add security policy:

      let child = spawn(
        `java`,
        [
          '-Dfile.encoding=UTF8',
          '-Djava.security.manager',
          '-Djava.security.policy==./funktions.policy ',
          '-jar'
          , runtTimePath
        ],
        process.env
      )
 */

const spawn = require('child_process').spawn
const constants = require('./constants')
const rf = require('./read-functions')

let getPath = (language) => {
  if(language === "kotlin") {
    return constants.kotlinRuntimePath
  }
  if(language === "wasm") {
    //let buff = new Buffer(sourceCode);
    //process.env.FUNCTION_CODE = buff.toString('base64');
    return constants.wasmRuntimePath
  } else {
    return constants.runtimePath
  }
}

function startFunction(funktionName, fastify) {
  const { redis } = fastify
  return new Promise((resolve, reject) => {
  
    // TODO: is it faster to call Redis?
    let funktionLang = rf.funktionsProjects[funktionName].language
    let funktionExtension = rf.funktionsProjects[funktionName].extension
    let funktionPort = rf.funktionsProjects[funktionName].port
    let funktionIsRunning = rf.funktionsProjects[funktionName].running
  
    if(!funktionIsRunning) {
      
      // projectName and funktionName are the same thing
      redis.get(`${constants.funktionsStorePrefix}:${funktionName}`, (error, jsonStringFunktionData) => {
        if (error) {
          reject({
            error: error
          })
        }
        let sourceCode
        try {
          sourceCode = JSON.parse(jsonStringFunktionData)["code"]
          // set environment variable for the JVM
          process.env.PORT = funktionPort
          process.env.LANG = funktionLang
          process.env.FUNCTION_CODE = sourceCode
  
          let runtTimePath = getPath(funktionLang)
  
          // TODO: check the signature of spawn
          let child = spawn(
            "java",
            ['-Dfile.encoding=UTF8', '-jar', runtTimePath],
            process.env
          )
          // TODO: is it faster to call Redis?
          // Update of the funktion in memory
          rf.funktionsProjects[funktionName].running = true
          rf.funktionsProjects[funktionName].pid = child.pid
          rf.funktionsProjects[funktionName].child = child
          
          resolve(rf.funktionsProjects[funktionName])
  
          child.on('exit', _ => {
            clearTimeout(timeOut)
            fastify.log.info('🤖 Function exited!')
          })
  
          child.stdout.on('data', (data) => {
            fastify.log.info(`🤖 ${data}`)
          })
  
          child.stderr.on('data', (data) => {
            fastify.log.error(`😡 ${data}`)
          })
  
          let timeOut = setTimeout( _ => {
            child.kill()
            rf.funktionsProjects[funktionName].running = false
            rf.funktionsProjects[funktionName].pid = ""
            rf.funktionsProjects[funktionName].startedAt = ""
            //rf.funktionsProjects[funktionName].lastCall = ""
            rf.funktionsProjects[funktionName].sinceLastCall = ""
            rf.funktionsProjects[funktionName].cpu = 0
            rf.funktionsProjects[funktionName].memory = 0
    
          }, constants.lifeDelay)
          
        } catch (exception) {
          reject({
            exception: exception
          })
        }
      })
      
    } else { // The funktion is already rinning
      resolve(rf.funktionsProjects[funktionName])
    }
  })
}

exports.startFunction = startFunction
