//TODO:
//  - refactoring (only one function with a optional parameter?)
//  - use object storage (Minio?)

const Table = require('easy-table')
const pidUsage = require('pidusage')

const counters = require('./counters')
const constants = require('./constants')

let funktionsProjects = {}

function readFunctions(fastify) {
  const { redis } = fastify
  
  return new Promise((resolve, reject) => {
    
    redis.keys(`${constants.funktionsStorePrefix}:*`).then(projectsKeys => {
  
      projectsKeys.forEach(funktionName => {
        redis.get(funktionName, (error, jsonStringFunktionData) => {
          if (error) {} // TODO
          // TODO: add a try catch
          let fk = JSON.parse(jsonStringFunktionData)
          
          let indexFile = fk["file"]
          let extension = fk["extension"]
          let language = fk["language"]
  
          funktionsProjects[funktionName.split(":")[1]] = {
            file: indexFile,
            language: language,
            extension: extension,
            port: counters.lastHttpPort+=1,
            pid: "",
            running: false,
            startedAt: "",
            lastCall: "",
            sinceLastCall: "",
            cpu: 0,
            memory: 0
          }
        })
      })
      resolve(funktionsProjects) // 👋 be careful to async => use await?
      
    }).catch(error => {
      reject({
        error: error
      })
    })
    
  })
}



function getUsage(member) {
  return pidUsage(funktionsProjects[member].pid).then(stats => {
    //console.log("🙂 ===>", funktionsProjects[member].pid, stats)
    return stats
  }).catch(error => {
    //console.log("😡 ===>", funktionsProjects[member].pid, error)
    return error
  })
}

// TODO: manage the error return
async function asyncCallGetUsage(member) {
  return await getUsage(member)
}

async function getFunctionsJsonList() {
  let outputFunktionsProjects = {}
  for(let member in funktionsProjects) { // funktionsProjects is like funktionsList
    outputFunktionsProjects[member] = {
      port: funktionsProjects[member].port,
      file: funktionsProjects[member].file,
      running: funktionsProjects[member].running,
      pid: funktionsProjects[member].pid,
      startedAt: funktionsProjects[member].startedAt,
      lastCall: funktionsProjects[member].lastCall,
      sinceLastCall: funktionsProjects[member].sinceLastCall
    }
    if(outputFunktionsProjects[member].running) {
      let result = await asyncCallGetUsage(member)
      //TODO: find why this is convert to string ?
      outputFunktionsProjects[member].cpu = result.cpu
      outputFunktionsProjects[member].memory = result.memory
    } else {
      outputFunktionsProjects[member].cpu = 0
      outputFunktionsProjects[member].memory = 0
    }
  }
  
  return outputFunktionsProjects
}

async function getFunctionsTable() {
  let lines = []
  
  let addLine = (member, status, cpu, memory) => {
    lines.push({
      funktion: member,
      port: funktionsProjects[member].port,
      file: funktionsProjects[member].file,
      running: funktionsProjects[member].running,
      pid: funktionsProjects[member].pid,
      status: status,
      cpu: `${cpu} %`,
      memory: `${memory} kilobytes`,
      startedAt: funktionsProjects[member].startedAt,
      lastCall: funktionsProjects[member].lastCall,
      sinceLastCall: funktionsProjects[member].sinceLastCall
    })
  }
  
  for(let member in funktionsProjects) {
    
    if(funktionsProjects[member].running) {
      let status = "🟢"
      
      // => {
      //   cpu: 10.0,            // percentage (from 0 to 100*vcore)
      //   memory: 357306368,    // bytes
      //   ppid: 312,            // PPID
      //   pid: 727,             // PID
      //   ctime: 867000,        // ms user + system time
      //   elapsed: 6650000,     // ms since the start of the process
      //   timestamp: 864000000  // ms since epoch
      // }
      let result = await asyncCallGetUsage(member)
      addLine(member, status, result.cpu.toFixed(2), (result.memory/1000).toFixed(2))
      
    } else {
      let status = "🔴"
      addLine(member, status, 0, 0)
    }
  }
  
  let t = new Table
  
  lines.forEach((item) => {
    t.cell('funktion', item.funktion)
    t.cell('port', item.port)
    t.cell('file', item.file)
    t.cell('running', item.running)
    t.cell('pid', item.pid)
    t.cell('status', item.status)
  
    t.cell('cpu', item.cpu)
    t.cell('memory', item.memory)
    
    t.cell('startedAt', item.startedAt)
    t.cell('lastCall', item.lastCall)
    t.cell('ms sinceLastCall', item.sinceLastCall)
    t.newRow()
  })
  return t.toString()
}

exports.readFunctions = readFunctions // Read from Redis
exports.getFunctionsJsonList = getFunctionsJsonList // Read from memory
exports.getFunctionsTable = getFunctionsTable // Read from memory
exports.funktionsProjects = funktionsProjects // Read (value) from memory