// TODO:
//   - add https support

// Require the framework and instantiate it
const fastify = require('fastify')({ logger: true })
const path = require('path')
const fetch = require('node-fetch')

const rf = require('./read-functions')

// ===== [START] Initialize data =====
const constants = require('./constants')
// ===== [END] Initialize data =====

fastify.register(require('fastify-static'), {
  root: path.join(__dirname, 'public'),
  prefix: '/'
})

fastify.register(require('fastify-formbody'))

fastify.register(require('./routes'), {
  funktionsStorePrefix: constants.funktionsStorePrefix
})

//TODO: we can use another Redis server
fastify.register(require('fastify-redis'), { url: constants.redisUrl, /* other redis options */ })


// Run the server!
const start = async () => {
  try {
    await fastify.listen(constants.storeHttpPort, constants.funktionsStore)
    fastify.log.info(`server listening on ${fastify.server.address().port}`)
  
    // ===== [START] Read functions =====
    rf.readFunctions(fastify)
      .then(data => {
        fastify.log.info(`🤖 functions list:`)
        fastify.log.info(rf.funktionsProjects)
        
        /*
          If the function is not before ${constants.inactiveLifeDelay}
          Then the store kill it
          Else the store kill it after ${constants.lifeDelay}
         */
      
        let timer = setInterval( _ => {
        
          for(let member in rf.funktionsProjects) {
            let funktion = rf.funktionsProjects[member]
            if(funktion.running) {
            
              let funktionUrl = `${constants.funktionsServerProtocol}://${constants.funktionsServer}:${funktion.port}`
            
              fetch(`${funktionUrl}/time`)
                .then(response => response.json())
                .then(data => {
                  fastify.log.info(`🟢 ${member} ${funktionUrl}/time ${JSON.stringify(data)}`)
                  //console.log("🟢", member, `${funktionUrl}/time`, data)
                  funktion.startedAt = data["startedAt"]
                  funktion.lastCall = data["lastCall"]
                  funktion.sinceLastCall = data["sinceLastCall"]
                  funktion.sinceLastCall = data["sinceLastCall"]
                
                  if(funktion.sinceLastCall>constants.inactiveLifeDelay) {
                    fastify.log.info(`🔴 killing ${member} [inactive]`)
                    //console.log("🔴", "killing", member, "[inactive]")
                    funktion.child.kill()
                    funktion.running = false
                    funktion.pid = ""
                    funktion.startedAt = ""
                    //funktion.lastCall = ""
                    funktion.sinceLastCall = ""
                    funktion.memory = 0
                    funktion.cpu = 0
                  }
                
                })
                .catch(error => {
                  //console.log("😡", error, member, funktion.language)
                  fastify.log.error(`😡 ${error} [${member}/${funktion.language}]`)
                  if(error.toString().startsWith("FetchError")) {
                    fastify.log.info("🤔 Try again...")
                  }
                  if(error.toString().startsWith("TypeError: Cannot read property 'kill' of undefined")) {
                    fastify.log.info("🤔 You probably remove (somewhere) the child property of the funktion")
                  }
                  //TypeError: Cannot read property 'kill' of undefined
            
                })
            }
          }
        }, constants.funktionsPollingDelay)
      
      })
      .catch(error => {
        fastify.log.error(error)
        process.exit(1)
      })
    // ===== [END] Read functions =====
    
  } catch (error) {
    fastify.log.error(error)
    process.exit(1)
  }
}
start()