#!/bin/bash
RUNTIME_PATH="../funky-graalvm-runtime/target/funky-graalvm-runtime-1.0.0-SNAPSHOT-fat.jar" \
KOTLIN_RUNTIME_PATH="../funky-kotlin-runtime/target/funky-kotlin-runtime-1.0.0-SNAPSHOT-fat.jar" \
WASM_RUNTIME_PATH="../funky-wasm-runtime/target/funky-wasm-runtime-1.0.0-SNAPSHOT-fat.jar" \
HTTP_PORT_START=9090 \
STORE_HTTP_PORT=8090 \
LIFE_DELAY=240000 \
INACTIVE_LIFE_DELAY=60000 \
FUNKTIONS_STORE_PREFIX="funktions" \
FUNKTIONS_STORE_TOKEN="SUxPVkVQQU5EQVMK" \
FUNKTIONS_STORE="0.0.0.0" \
FUNKTIONS_SERVER="127.0.0.1" \
FUNKTIONS_SERVER_PROTOCOL="http" \
FUNKTIONS_POLLING_DELAY=5000 \
REDIS_URL="redis://127.0.0.1" \
npm start

# FUNKTIONS_SERVER: where the funktions are running
# most of the time it's a the same place as the store

