// ===== add funKtion =====
const constants = require('./constants')
const counters = require('./counters')
const rf = require('./read-functions')

// TODO:
//   - add a token to be able to create a funktion
//   - test posted values

function addFunction(data, fastify) {
  const { redis } = fastify
  return new Promise((resolve, reject) => {
    
    let projectName = data.name
    let language = data.language
    let extension = data.extension
    let fileName = `index${extension}`
    let code = data.code
    
    let fk = {
      file: fileName,
      language: language,
      extension: extension,
      code: code
    }
    // TODO: I don't need to store everything in Redis
    // projectName and funktionName are the same thing
    redis.set(`${constants.funktionsStorePrefix}:${projectName}`, JSON.stringify(fk), (error) => {
      if (error) {
        reject({
          error: error
        })
      }
      // TODO: is it faster to call Redis?
      // Refresh: add the funktion in memory
      rf.funktionsProjects[projectName] = {
        file: fileName,
        language: language,
        extension: extension,
        port: counters.lastHttpPort+=1,
        pid: "",
        running: false
      }
  
      resolve(rf.funktionsProjects[projectName])
      
    })
    
  })
}

exports.addFunction = addFunction
