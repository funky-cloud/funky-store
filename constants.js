
const RUNTIME_PATH = process.env.RUNTIME_PATH
const KOTLIN_RUNTIME_PATH = process.env.KOTLIN_RUNTIME_PATH
const WASM_RUNTIME_PATH = process.env.WASM_RUNTIME_PATH
const HTTP_PORT_START = parseInt(process.env.HTTP_PORT_START || "9090")
const STORE_HTTP_PORT = parseInt(process.env.STORE_HTTP_PORT || "8090")

const LIFE_DELAY = parseInt(process.env.LIFE_DELAY || 60000) // milliseconds
const INACTIVE_LIFE_DELAY = parseInt(process.env.INACTIVE_LIFE_DELAY || 30000) // milliseconds
const FUNKTIONS_POLLING_DELAY = parseInt(process.env.FUNKTIONS_POLLING_DELAY || 5000) // milliseconds

const FUNKTIONS_STORE_PREFIX = process.env.FUNKTIONS_STORE_PREFIX || "funktions"
const FUNKTIONS_STORE = process.env.FUNKTIONS_STORE || "0.0.0.0"
const FUNKTIONS_SERVER = process.env.FUNKTIONS_SERVER || "127.0.0.1"
const FUNKTIONS_SERVER_PROTOCOL = process.env.FUNKTIONS_SERVER_PROTOCOL || "http"

const FUNKTIONS_STORE_TOKEN = process.env.FUNKTIONS_STORE_TOKEN || "ILOVEPANDAS"

const REDIS_URL = process.env.REDIS_URL || "redis://127.0.0.1"

const constants = {
  ".js": "js", ".py": "python", ".rb": "ruby", ".kt": "kotlin", ".wasm": "wasm"
}

exports.runtimePath = RUNTIME_PATH
exports.kotlinRuntimePath = KOTLIN_RUNTIME_PATH
exports.wasmRuntimePath = WASM_RUNTIME_PATH
exports.httpPortStart = HTTP_PORT_START
exports.storeHttpPort = STORE_HTTP_PORT

exports.lifeDelay = LIFE_DELAY
exports.inactiveLifeDelay = INACTIVE_LIFE_DELAY
exports.funktionsPollingDelay = FUNKTIONS_POLLING_DELAY

exports.funktionsStorePrefix = FUNKTIONS_STORE_PREFIX
exports.funktionsStore = FUNKTIONS_STORE
exports.funktionsServer = FUNKTIONS_SERVER
exports.funktionsServerProtocol = FUNKTIONS_SERVER_PROTOCOL

exports.funktionsStoreToken = FUNKTIONS_STORE_TOKEN

exports.redisUrl = REDIS_URL

exports.languages = constants